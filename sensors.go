package main

import (
	"database/sql"
	"fmt"
	"net/http"
	
	_ "github.com/go-sql-driver/mysql"
	"github.com/labstack/echo/v4"
)

type SensorDatum struct {
    SensorValue string `json:"sensorValue" xml:"sensorValue"`
    ID1 string `json:"Id1" xml:"Id1"`
    ID2 string `json:"Id2" xml:"Id2"`
    Timestamp string `json:"timestamp" xml:"timestamp"`
  }

type SensorData struct {
			SensorData []SensorDatum `json:"sensorData" xml:"sensorData"`
}

type Excuse struct {
    Status string `json:"status" xml:"status"`
}


func saveSensorDatum(c echo.Context) error {

	db, err := sql.Open("mysql", "root:t00r@tcp(localhost:3306)/rest_example")

    if err != nil {
    	fmt.Println(err.Error())
     	response := Excuse{Status: err.Error()}
     	return c.JSON(http.StatusInternalServerError, response)
    }

    defer db.Close()

	payload := new(SensorDatum)
	if err := c.Bind(payload); err != nil {
		fmt.Print("error binding")
				return err
	}
	//
	sql := "INSERT INTO sensor_data(sensor_value, id1, id2, timestamp) VALUES( ?, ?, ?, ?)"
	stmt, err := db.Prepare(sql)

	if err != nil {
		fmt.Print("error prepare")
		fmt.Print(err.Error())
	}
	defer stmt.Close()

	_, err2 := stmt.Exec(payload.SensorValue, payload.ID1, payload.ID2, payload.Timestamp)

	// Exit if we get an error
	if err2 != nil {
		fmt.Println("error exec")
		fmt.Println(err2.Error())
		response := Excuse{Status: err2.Error()}

		return c.JSONPretty(http.StatusInternalServerError, response, "  ")
	} 

   	response := Excuse{Status: "Succesfully saving new sensor datum"}

	return c.JSONPretty(http.StatusOK, response, "  ")
	
	
}

func  retrieveSensorData(c echo.Context) error {

	var sensorRecords []SensorDatum
	sensorRecords = make([]SensorDatum, 0)

	paramStartTimestamp := c.QueryParam("start_timestamp")
	paramEndTimestamp := c.QueryParam("end_timestamp")

	paramId1 := c.QueryParam("ID1")
	paramId2 := c.QueryParam("ID2")
	
	var dbConnectionString = cfg.Database.MySQL.Username + ":" + cfg.Database.MySQL.Password + "@tcp(" + cfg.Database.MySQL.Host + ":" + cfg.Database.MySQL.Port + ")/" + cfg.Database.MySQL.DatabaseName

	db, err := sql.Open(cfg.Database.Driver, dbConnectionString)

    if err != nil {
    	fmt.Println(err.Error())
     	response := Excuse{Status: err.Error()}
     	return c.JSON(http.StatusInternalServerError, response)
    } else {
    	fmt.Println("db is connected")
    }

    defer db.Close()

    rows, err := db.Query("SELECT sensor_value, id1, id2, timestamp FROM sensor_data WHERE `timestamp` >= FROM_UNIXTIME(?) and `timestamp` <= FROM_UNIXTIME(?)", paramStartTimestamp, paramEndTimestamp)

    if paramId1 != "" && paramId2 != "" {
    	rows, err = db.Query("SELECT sensor_value, id1, id2, timestamp FROM sensor_data WHERE id1 = ? and id2 = ?", paramId1, paramId2)
    }

    if err != nil {
    	fmt.Println(err.Error())
     	response := Excuse{Status: err.Error()}
     	return c.JSON(http.StatusInternalServerError, response)
	} 

	defer rows.Close()
	
	for rows.Next() {

	    var sensorValue string
		var id1 string
		var id2 string
		var timeStamp string

	    err = rows.Scan(&sensorValue, &id1, &id2, &timeStamp)

	    fetchedSensorDatum := SensorDatum{SensorValue:sensorValue, ID1:id1, ID2:id2, Timestamp:timeStamp}
	    sensorRecords = append(sensorRecords, fetchedSensorDatum)
	    if err != nil {
	    	fmt.Println(err.Error())
	     	response := Excuse{Status: err.Error()}
	     	return c.JSON(http.StatusInternalServerError, response)
		} 
	}
	 
	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
	    panic(err)
	}

	var sensorData SensorData

	sensorData = SensorData{SensorData:sensorRecords}

	c.Response().Header().Set(echo.HeaderContentType, echo.MIMEApplicationJSONCharsetUTF8)
  	c.Response().WriteHeader(http.StatusOK)


	return c.JSONPretty(http.StatusOK, sensorData, "  ")
}
