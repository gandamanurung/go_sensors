package main

type Config struct {
    Server struct {
        Port string `yaml:"port"`
    } `yaml:"server"`
    Database struct {
        Driver string `yaml:"driver"`
        MySQL struct {
            Host string `yaml:"host"`
            Port string `yaml:"port"`
            DatabaseName string `yaml:"dbname"`
            Username string `yaml:"user"`
            Password string `yaml:"pass"`
        } `yaml:"mysql"`
    } `yaml:"database"`
}

