package main

import (
	"bytes"
   	"encoding/json"
   	"io/ioutil"
   	"log"
   	"net/http"
   	"math/rand"
   	"time"
   	"strconv"
)

var letters = []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZ")

func randSeq(n int) string {
	rand.Seed(time.Now().UnixNano())
    b := make([]rune, n)
    for i := range b {
        b[i] = letters[rand.Intn(len(letters))]
    }
    return string(b)
}

func randomInteger(min int, max int) string {
	rand.Seed(time.Now().UnixNano())
	newRandom := min + rand.Intn(max-min)
	return strconv.Itoa(newRandom)
}

func generateSensorData() {


	postBody, _ := json.Marshal(map[string]string{
      "sensorValue":  randomInteger(1,100),
      "Id1": randomInteger(1,10),
      "Id2": randSeq(1),
      "Timestamp" : time.Now().Format("2006-01-02 15:04:05"),
   })

   log.Println("Request:", string(postBody))

	responseBody := bytes.NewBuffer(postBody)
	resp, err := http.Post("http://localhost:" + cfg.Server.Port +"/data", "application/json", responseBody)
	
	if err != nil {
	    log.Fatalf("An Error Occured %v", err)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
	    log.Fatalln(err)
	}

	sb := string(body)
	log.Printf(sb)
 
}
		
