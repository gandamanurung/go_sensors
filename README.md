# Go Sensor
_Created For Job Interview Coding Assignment (Senior Backend Developer)_


## Steps To Install and Run the Program
1. Install Go / Golang (Go Programming Language)
2. Install MySQL Server
3. Install MySQL Client (SQLYog, DBeaver, Navicat, phpMyAdmin, etc)
4. Unzip the `go_sensors.zip` to specific directory
5. Import the schema to MySQL database using MySQL Client, by executing script database.sql
6. Open Command Prompt and go to the unzipped go_sensors folder (for example: `c:\go_sensors`)
7. Execute below command on command prompt / shell
```sh
go get github.com/labstack/echo/v4
go get github.com/labstack/echo/v4/middleware
go get gopkg.in/yaml.v2
go get gopkg.in/robfig/cron.v2
go get github.com/go-sql-driver/mysql
```
8. Open `config.yml` file with your favorite text-editor and change the username, password or port to your MySQL installation configuration. Usually, by default, by default it will have root username with empty password. For example:
```yaml
database:
  driver: "mysql"
  mysql:
    host: "localhost"
    port: "3306"
    user: "root"
    pass: ""
    dbname: "sensors"
```
9. Execute the go sensor program by executing command
```
go run .
```
10. Install and open Postman or any other REST Client

## Sensor Data Generator
This is an automated process inside the `main()` function of go sensor program to save mock data. Every 5 seconds it will execute `generateSensorData()` which call an HTTP POST request with random sensor data to  `http://localhost:<port>/data`.  

## Step to Test
### Save a New Sensor Datum

Open Postman or other REST Client, create a `POST` request to `http://localhost:1323/data` with below sample data
```json
{
    "sensorValue": "13",
    "Id1": "2",
    "Id2": "A",
    "timestamp": "2022-01-22 01:01:11"
}
```
Above REST POST Endpoint will output:
```json
{
    "status": "Succesfully saving new sensor datum"
}
```

### Fetch Sensor Data
Open Postman or other REST Client, create a `GET` request
#### Filter by Timestamp
Hit GET request to `http://localhost:1323/data?start_timestamp=1641072081&end_timestamp=1643664082`
Fill `start_timestamp` and `end_timestamp` with the correct value.

Sample output:
```json
{
    "sensorData": [
        {
            "sensorValue": "43",
            "Id1": "1",
            "Id2": "A",
            "timestamp": "2022-01-11 21:21:21"
        },
        {
            "sensorValue": "12",
            "Id1": "2",
            "Id2": "A",
            "timestamp": "2022-01-22 01:01:11"
        }
    ]
}
```

#### Filter by ID11 and ID2
Hit GET request to `http://localhost:1323/data?ID1=1&ID2=A`
Fill `ID1` and `ID2` with the correct value.

Sample output:
```json
{
    "sensorData": [
        {
            "sensorValue": "43",
            "Id1": "1",
            "Id2": "A",
            "timestamp": "2022-01-11 21:21:21"
        },
        {
            "sensorValue": "43",
            "Id1": "1",
            "Id2": "A",
            "timestamp": "2022-01-11 21:21:21"
        }
    ]
}
```
