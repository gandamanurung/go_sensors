CREATE TABLE `sensor_data` (
  `sensor_value` INTEGER NOT NULL,
  `id1` INTEGER NOT NULL,
  `id2` varchar(3) NOT NULL,
  `timestamp` timestamp NOT NULL
);