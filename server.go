package main

import (
	
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gopkg.in/yaml.v2"
	"gopkg.in/robfig/cron.v2"

	"os"
	"fmt"

)

var configFile os.File
var configFileError error
var cfg Config

func processError(err error) {
    fmt.Println(err)
    os.Exit(2)
}

func main() {

	// Echo instance
	e := echo.New()

	configFile, configFileError := os.Open("config.yml")
	if configFileError != nil {
     	processError(configFileError)
	}

	defer configFile.Close()

	decoder := yaml.NewDecoder(configFile)
	configFileError = decoder.Decode(&cfg)
	if configFileError != nil {
     	processError(configFileError)
	}


	cronJob := cron.New()
	cronJob.AddFunc("@every 5s", generateSensorData)
	cronJob.Start()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
	    AllowOrigins: []string{"*"},
	    AllowMethods: []string{echo.GET, echo.PUT, echo.POST, echo.DELETE},
	}))

	e.GET("/data", retrieveSensorData)
	e.POST("/data", saveSensorDatum)


	e.Logger.Fatal(e.Start(":" + cfg.Server.Port))
}